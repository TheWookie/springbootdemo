package com.wookie.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wookie.dao.ContactsDao;
import com.wookie.entities.Contacts;

//@Repository
public class ContactsDaoImpl implements ContactsDao {

	@PersistenceContext
	private EntityManager entityManager; // may use unwrap(...) to work with hibernate's "Session" or read about Static metamodels.
	
	@Override
	public Contacts getByEmail(String email) {
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Contacts.class)
							.add(Restrictions.like("email", email));
		
		Contacts result = (Contacts) criteria.uniqueResult();
		
		return result;
	}

}
