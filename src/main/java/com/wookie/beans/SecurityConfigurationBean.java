package com.wookie.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

@EnableWebSecurity
public class SecurityConfigurationBean extends WebSecurityConfigurerAdapter {

	@Autowired
	UserDetailsService securityService;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
			.authorizeRequests()   
				.antMatchers("/", "/api/**").permitAll()                  
				.antMatchers("/admin/**").hasRole("ADMIN") // Spring always add ROLE_(Shift-) before the actual role name!!!
				.and().formLogin();
		
//		 http.authorizeRequests().antMatchers("/", "/list")
//         .access("hasRole('USER') or hasRole('ADMIN') or hasRole('DBA')")
//         .antMatchers("/newuser/**", "/delete-user-*").access("hasRole('ADMIN')").antMatchers("/edit-user-*")
//         .access("hasRole('ADMIN') or hasRole('DBA')").and().formLogin().loginPage("/login")
//         .loginProcessingUrl("/login").usernameParameter("ssoId").passwordParameter("password").and()
//         .rememberMe().rememberMeParameter("remember-me").tokenRepository(tokenRepository)
//         .tokenValiditySeconds(86400).and().csrf().and().exceptionHandling().accessDeniedPage("/Access_Denied");

	}
}
