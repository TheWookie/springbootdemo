package com.wookie.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;

import com.wookie.service.CalculationService;

@Component
public class Model {
//	@Autowired
	private CalculationService calculationService;

	@Autowired
//	@Required
	public void setCalculationService(CalculationService calculationService) {
		System.out.println("In model setter " + calculationService);
		this.calculationService = calculationService;
		System.out.println("AAAA: " + this.calculationService);
	}
	
	
	public Integer performCalculation() {
		System.out.println("AAAA: " + calculationService);
		return calculationService.calculate();
	}
}
