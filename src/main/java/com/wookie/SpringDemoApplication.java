package com.wookie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


//@Configuration
//@ComponentScan
//@EnableAutoConfiguration
@SpringBootApplication
public class SpringDemoApplication {//extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(SpringDemoApplication.class, args);
	}
	
//	@Override
//	protected final SpringApplicationBuilder configure(final SpringApplicationBuilder application) {
//		return application.sources(SpringDemoApplication.class);
//	}
}
