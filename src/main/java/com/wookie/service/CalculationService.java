package com.wookie.service;

import java.util.List;

import com.wookie.entities.Contacts;

public interface CalculationService {
	Integer calculate();
	List<Contacts> getAll();
}
