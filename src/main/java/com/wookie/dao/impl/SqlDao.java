package com.wookie.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.wookie.dao.MyDao;
import com.wookie.entities.Contacts;

public class SqlDao implements MyDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public boolean create() {
		System.out.println("Created");
		return true;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Contacts> getAll() {
		return entityManager.createQuery("from Contacts").getResultList();
	}

}
