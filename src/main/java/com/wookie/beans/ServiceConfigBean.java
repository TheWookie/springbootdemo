package com.wookie.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.wookie.model.Model;
import com.wookie.service.CalculationService;
import com.wookie.service.impl.MultService;
import com.wookie.service.impl.SumService;

@Configuration
@ComponentScan(value = {"com.wookie.service"})
public class ServiceConfigBean {

//	@Bean
//	public Model getModel() {
//		return new Model();
//	}
	
	
	@Bean
	public CalculationService getCalculationService() {
		System.out.println("HERE1");
//		return new SumService();
		return new MultService();
	}
	
}
