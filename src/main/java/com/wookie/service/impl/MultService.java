package com.wookie.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.wookie.dao.MyDao;
import com.wookie.entities.Contacts;
import com.wookie.service.CalculationService;

//@Service
public class MultService implements CalculationService{

	private MyDao dao;
	
	@Override
	public Integer calculate() {
		dao.create();
		Integer res = 2 * 3;
		return res;
	}

	@Autowired
	public void setDao(MyDao dao) {
		this.dao = dao;
	}

	@Override
	public List<Contacts> getAll() {
		return dao.getAll();
	}
}
