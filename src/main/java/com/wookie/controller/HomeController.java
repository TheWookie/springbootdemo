package com.wookie.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.wookie.model.Model;
import com.wookie.service.CalculationService;

@Controller
@EnableAutoConfiguration
public class HomeController {
//	private Model model;
	private CalculationService calcService;
	
	@ResponseBody
    @RequestMapping(value = "/api/getSearchResult")
    public String index(@RequestBody String search) {
    	System.out.println("TEST: " + search);
    	System.out.println("TEST: " + calcService.getAll());
    	System.out.println("TEST2: " + calcService.getAll().toString());
    	
    	
    	// logout
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	if(auth != null) {
    		SecurityContextHolder.getContext().setAuthentication(null);
    	}
    	//
    	
    	
		return calcService.calculate().toString();
//    	return "AA";
    }
	
	@ResponseBody
    @RequestMapping(value = "/api/test")
    public String testREST() {
		
    	return "AA";
    }

	@ResponseBody
    @RequestMapping(value = "/admin")
    public String admin() {
		
    	return "AADMIN";
    }
	
	@RequestMapping(value = "/")
	public String home() {
		return "redirect:/index.html";
	}
	
	@Autowired
	public void setCalcService(CalculationService calcService) {
		this.calcService = calcService;
	}

}
