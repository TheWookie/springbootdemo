package com.wookie.dao;

import com.wookie.entities.Contacts;

public interface ContactsDao {
	
	Contacts getByEmail(String email);
	
}
