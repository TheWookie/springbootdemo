package com.wookie.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.wookie.dao.ContactsDao;
import com.wookie.entities.Contacts;

/*
 * Service for managing Spring Security.
 */
@Service
public class SecurityUserDataServiceImpl implements UserDetailsService {
	ContactsDao contactsDao;
	
	@Override
	@Transactional(readOnly=true)
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		System.out.println("!!loadUserByUsername Service");
		Contacts contacts = contactsDao.getByEmail(email);
		
		return new org.springframework.security.core.userdetails.User(
				contacts.getEmail(), contacts.getPassword(), 
                true, true, true, true, getGrantedAuthorities(contacts));
	}
	
	private List<GrantedAuthority> getGrantedAuthorities(Contacts contacts){
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        
        System.out.println("!!getGrantedAuthorities Service " + contacts.getRole().getName());
        
        authorities.add(new SimpleGrantedAuthority(
        		"ROLE_"+contacts.getRole().getName()));
//        		"ROLE_ADMIN"));
        return authorities;
    }

	@Autowired
	public void setContactsDao(ContactsDao contactsDao) {
		this.contactsDao = contactsDao;
	}
	
}
