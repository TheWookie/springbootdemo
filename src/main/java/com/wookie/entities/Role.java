package com.wookie.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Role implements java.io.Serializable {

	@Id
	@GeneratedValue
	private Integer id;
	private String name;
	
	// Should eliminate problem: "Could not determine type for: java.util.Set, at table: ..."
	@Column
    @ElementCollection(targetClass=Contacts.class)
	private Set contactses = new HashSet(0);

	public Role() {
	}

	public Role(String name) {
		this.name = name;
	}

	public Role(String name, Set contactses) {
		this.name = name;
		this.contactses = contactses;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set getContactses() {
		return this.contactses;
	}

	public void setContactses(Set contactses) {
		this.contactses = contactses;
	}

}
