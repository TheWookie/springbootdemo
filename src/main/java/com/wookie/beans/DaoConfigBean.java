package com.wookie.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.wookie.dao.*;
import com.wookie.dao.impl.*;

@Configuration
@ComponentScan(value = {"com.wookie.dao"})
public class DaoConfigBean {

	@Bean
	public MyDao getDao() {
		System.out.println("Here DAo");
		return new SqlDao();
	}
	
	@Bean 
	public ContactsDao getContactsDao() {
		System.out.println("BEAN CONTACTSDAO");
		return new ContactsDaoImpl();
	}
	
}
