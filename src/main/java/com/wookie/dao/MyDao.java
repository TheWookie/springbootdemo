package com.wookie.dao;

import java.util.List;

import com.wookie.entities.Contacts;

public interface MyDao {
	boolean create();
	List<Contacts> getAll();
}
